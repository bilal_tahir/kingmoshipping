
<!-- Mirrored from www.kosshipping.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 12 May 2020 22:20:13 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>

    <!-- Metadata & Basic Page Needs -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>KINGSMO SHIPPING</title>
    <meta name="autor" content="www.geniuscodes.com" />
    <meta name="copyright" content="Genius Codes 2016" />
    <meta name="description" content="Integrated logistics services and tailored, customer-focused solutions for imports and exports, cargo handling, courier services, warehousing, transportation and transit challenges." />
    <meta name="keywords" content="Ghana Freight, Ghana Logistics, Ghana Cargo, Ghana Shipping,Tema Freight, Tema Logistics, Tema Cargo, Tema Shipping, Freight, Forwarding, Shipping, Cargo, Exports & Important Ghana, Imports & Exports, Cargo Handling, Ghana, Africa, Best In Ghana" />

    <!-- Favicon -->
    
    <link rel="apple-touch-icon" sizes="57x57" href="apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="manifest.json">
    <link rel="mask-icon" href="safari-pinned-tab.html" color="#0a599e">
    <meta name="apple-mobile-web-app-title" content="Kos Shipping">
    <meta name="application-name" content="Kos Shipping">
    <meta name="msapplication-TileColor" content="#0a599e">
    <meta name="msapplication-TileImage" content="mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- Load CSS Files -->
    <link href="css/main.css" rel="stylesheet" type="text/css">
    <link href="css/lightgallery.css" rel="stylesheet">

    <!-- Slider -->
    <link rel="stylesheet" href="css/slick.css">

    <!-- Lightbox -->
    <link rel="stylesheet" href="css/magnific.css">



    
     @yield('css')
        

</head>
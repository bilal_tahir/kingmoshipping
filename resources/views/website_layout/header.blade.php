   <!-- header begin -->
        <header>
            <div class="container">
                <span id="menu-btn"></span>

                <div class="row">
                    <div class="col-md-3">

                        <!-- logo begin -->
                        <div id="logo">
                            <div class="inner">
                                <a href="{{url('/')}}">
                                <img src="{{asset('/storage'.'/'.$logo->logo)}}" alt="" class="logo-1" style="height: 75px !important; width:250px !important;">
                                    <img src="{{asset('/storage'.'/'.$logo->logo)}}" alt="" class="logo-2" >
                                </a>

                            </div>
                        </div>
                        <!-- logo close -->

                    </div>

                    <div class="col-md-9">

                        <!-- mainmenu begin -->
                        <nav id="mainmenu-container">
                            <ul id="mainmenu">

                                <li><a href="{{url('/')}}">Home</a>
                                </li>

                            <li><a href="{{url('/about')}}">About Us</a>
                                    <ul>
                                        <li><a href="{{url('/portfolio')}}">Client Portfolio</a></li>
                                        <li><a href="{{url('/team')}}">Meet The Team</a></li>
                                    </ul>
                                </li>

                            <li><a href="{{url('/services')}}">Our Services</a>
                                    <ul>
                                    <li><a href="{{url('/landfreight')}}">Land Freight</a></li>
                                        <li><a href="{{url('/airfreight')}}">Air Freight</a></li>
                                        <li><a href="{{url('/seafreight')}}">Sea Freight</a></li>
                                        <li><a href="{{url('/solution')}}">Freight Forwarding Solutions</a></li>
                                        <li><a href="{{url('/package')}}">Business Travel Package</a></li>
                                    </ul>
                                </li>

                                <li><a href="{{url('/contact')}}">Contact Us</a>
                                    <ul>
                                        <li><a href="{{url('/quote')}}">Get A Quote</a></li>
                                    </ul>
                                </li>

                            </ul>
                        </nav>
                        <!-- mainmenu close -->

                        <!-- search -->
                        <div class="search text-right">
                            <input type="text" name="search" id="search" placeholder="search">
							<button type="submit" class="btn-search-icon">
								<i class="fa fa-search"></i>
							</button>
                        </div>
                        <!-- social icons close -->

                    </div>
                </div>
            </div>
        </header>
        <!-- header close -->

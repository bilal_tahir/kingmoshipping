<!DOCTYPE html>
<html lang="en">

@include('website_layout.head')

<body>

    <!-- Google Tag Manager -->
    <noscript><iframe src="http://www.googletagmanager.com/ns.html?id=GTM-MH7TSF"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'www.googletagmanager.com/gtm54455445.html?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-MH7TSF');</script>
    <!-- End Google Tag Manager -->

    <div id="preloader"></div>
    <div id="wrapper">


    @include('website_layout.header')

    @yield('content')

    

    @include('website_layout.footer')

</div>

        <!-- Load JS Files -->
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.isotope.min.js"></script>
        <script src="js/easing.js"></script>
        <script src="js/jquery.ui.totop.js"></script>
        <script src="js/selectnav.html"></script>
        <script src="js/ender.js"></script>
        <script src="js/owl.carousel.js"></script>
        <script src="js/jquery.fitvids.js"></script>
        <script src="js/jquery.plugin.js"></script>
        <script src="js/wow.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/tweecool.js"></script>
        <script src="js/jquery.stellar.js"></script>
        <script src="js/typed.js"></script>

        <!-- Theme and Plugins -->
        <script src="js/custom.js"></script>
        <script src="js/custom-tweecool.js"></script>

         <script type="text/javascript">
        $(document).ready(function(){
            $('#lightgallery').lightGallery();
        });
        </script>
        <script src="https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js"></script>
        <script src="js/lightgallery-all.min.js"></script>
        <script src="js/jquery.mousewheel.min.js"></script>

		<!--Start of Tawk.to Script-->
		<script type="text/javascript">
		var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
		(function(){
		var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
		s1.async=true;
		s1.src='https://embed.tawk.to/57fe92e2cfdf421cf960ab71/default';
		s1.charset='UTF-8';
		s1.setAttribute('crossorigin','*');
		s0.parentNode.insertBefore(s1,s0);
		})();
		</script>
		<!--End of Tawk.to Script-->

  </body>
</html>
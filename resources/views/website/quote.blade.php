@extends('website_layout.main')
@section('css')
 <style>
    #section-welcome-2{background: url('storage/{{$q->sliderimage}}') fixed; background-size:cover;}
    </style>
 @endsection
 @section('content')

        <!-- subheader begin -->
       <div class="carousel-inner" >
            
             <div class="carousel-item" >
                <img src="{{asset('storage/'.$q->sliderimage)}}" alt="..." class="slider" >
                <div class="carousel-caption d-none d-md-block carousel-contents">
                    {!!($q->image_heading)!!}
                   {{-- <a href="{{url('/quote')}}" class="btn btn-custom wow fadeInUp" data-wow-duration="2s">Get A Quote</a> --}}
                </div>
                </div>

        </div>
        <!-- subheader close -->



        <!-- content begin -->
        <div id="content" class="no-top no-bottom cusa">

            <section id="section-contact-form">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="light-text">
                                <div class="bg-2 padding30">

                                    <h2 class="id-color">{{$q->form_heading}}</h2>
                                    <div class="tiny-border"></div>
                                    <p class="lead big">
                                        <i>{{$q->form_text}}</i>
                                    </p>
                                    <div class="text-center">
                                        <img src="img/contact/fork.png" alt="">
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-8">
                        <form id="contact-form" class="row form-transparent" method="post" action="{{url('/qmessage')}}" enctype="multipart/form-data">
@csrf
                                <div class="col-md-6">
									<div id="error_email" class="error">Please Input a name</div>
                                    <input type="text" name="name" class="form-control" placeholder="Full Name" required="required">
                                </div>

                                <div class="col-md-6">
                                    <div id="error_email" class="error">Please check your email</div>
                                    <input type="email" name="email" class="form-control" placeholder="Email" required="required">
                                </div>

                                <div class="col-md-6">
                                    <div id="error_email" class="error">Please check your number</div>
                                    <input type="text" name="mobile" class="form-control" placeholder="mobile" required="required">
                                </div>

                                <div class="col-md-12">
                                    <div id="error_message" class="error">Please check your message</div>
                                    <textarea rows="6" name="message" class="form-control" placeholder="Your Message" required="required"></textarea>
                                </div>

                                <div id="mail_success" class="col-md-12 success">Thank you for contacting us. We will be in touch with you very soon.</div>
                                <div id="mail_failed" class="col-md-12 error">Error, email not sent</div>

                                <div class="col-md-12">
                                    <p id="btnsubmit">
										<button type="submit" value="Send message" name="submit" id="submit" class="btn btn-customm fullwidth" title="Submit Your Message!">Send Message</button>
                                    </p>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </section>

        </div>
    </div>
    <!-- content close -->

 @endsection
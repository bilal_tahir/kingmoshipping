@extends('website_layout.main')
@section('css')
 <style>
    #section-welcome-2{background: url('storage/{{$ab->sliderimage}}') fixed; background-size:cover;}
    </style>
 @endsection
 @section('content')
  <!-- subheader begin -->
        <div class="carousel-inner" style="background-color: #eee !important;">
            
             <div class="carousel-item" style="background-color: #eee !important;">
                <img src="{{asset('storage/'.$ab->sliderimage)}}" alt="..." class="slider" >
                <div class="carousel-caption d-none d-md-block carousel-contents">
                    {!!($ab->image_heading)!!}
                   {{-- <a href="{{url('/quote')}}" class="btn btn-custom wow fadeInUp" data-wow-duration="2s">Get A Quote</a> --}}
                </div>
                </div>

        </div>
        <!-- subheader close -->

        <div class="clearfix"></div>

        <!-- content begin -->
        <div id="content" class="no-padding cusa">

            <!-- section begin -->
            <section class="bg-grey">
                <div class="container">
                    <div class="row">

                        <div class="col-md-4">

                        <h2>{{$ab->first_section_heading}}</h2>
                            <div class="divider-deco"><span></span></div>
                          {!!html_entity_decode($ab->first_section_content)!!}
                        </div>

                        <div class="col-md-4">
                            <div class="padding20 light-text">
                                <div class="bg-2 padding30">

                                    <h2 class="id-color">{{$ab->second_section_heading}}</h2>
                                    <div class="tiny-border"></div>
                                    {!!html_entity_decode($ab->second_section_content)!!}
                                  
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                </div>
                            <img src="{{asset('storage/'.$ab->second_section_image)}}" class="img-custom-1" alt="" style="width:280px !important; height:180px !important;">
                            </div>

                        </div>

                        <div class="col-md-4 cust">
                            <h2>{{$ab->third_section_heading}}</h2>
                            <div class="divider-deco"><span></span></div>
                            {!!html_entity_decode($ab->third_section_content)!!}


                        </div>

                    </div>


                </div>
            </section>

            <section id="section-with-bg-2 " data-stellar-background-ratio="0.5">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="overlay80 padding40">
                                <div class="row">
                                    <div class="col-md-12">

                                    <h2>{{$ab->bottom_heading}}</h2>
                                       {!!html_entity_decode($ab->bottom_first_description)!!}
                                        <div class="divider-deco"><span></span></div>
                                       {!!html_entity_decode($ab->bottom_second_description)!!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <!-- content close -->

 @endsection
@extends('website_layout.main')
@section('css')
 <style>
    #section-welcome-2{background: url('storage/{{$ser->sliderimage}}') fixed; background-size:cover;}
    </style>
 @endsection
 @section('content')

        <!-- subheader begin -->
         <div class="carousel-inner" >
            
             <div class="carousel-item" >
                <img src="{{asset('storage/'.$ser->sliderimage)}}" alt="..." class="slider" >
                <div class="carousel-caption d-none d-md-block carousel-contents">
                    {!!($ser->image_heading)!!}
                   {{-- <a href="{{url('/quote')}}" class="btn btn-custom wow fadeInUp" data-wow-duration="2s">Get A Quote</a> --}}
                </div>
                </div>

        </div>
        <!-- subheader close -->

        <div class="clearfix"></div>

        <!-- content begin -->
        <div id="content" class="no-top no-bottom cusa">
            <section>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>{{$ser->page_heading}}</h2>
                           {!!html_entity_decode($ser->first_description)!!}
                            <div class="divider-deco"><span></span></div>
                          {!!html_entity_decode($ser->second_description)!!}

                        </div>

                        <div class="divider-single"></div>
                        <div class="divider-half"></div>

                    </div>
                </div>
            </section>

            <!-- section begin -->
            <section class="no-padding">
                <div class="container-fullwidth">
         @foreach($freight as $f)
                    <div class="box-service one-fourth">

                        <div class="bg-color-fx padding-5 text-center">
                        <h3>{{$f->heading}}</h3>
                            <div class="tiny-border margintop10 marginbottom10"></div>
                        <img src="{{asset('storage/'.$f->image)}}" class="img-responsive margintop20 marginbottom20 wow fadeInRight" alt="" />
                           {!!html_entity_decode($f->text)!!}
                                             @if($f->id == 2)
                        <a href="{{url('/airfreight')}}" class="btn-arrow hover-light"><span class="line"></span><span class="url">Read More</span></a>
                       @endif
                        @if($f->id == 3)
                        <a href="{{url('/seafreight')}}" class="btn-arrow hover-light"><span class="line"></span><span class="url">Read More</span></a>
                        @endif  
                        @if($f->id == 4)
                        <a href="{{url('/solution')}}" class="btn-arrow hover-light"><span class="line"></span><span class="url">Read More</span></a>
                        @endif    
                        @if($f->id == 5)
                        <a href="{{url('/package')}}" class="btn-arrow hover-light"><span class="line"></span><span class="url">Read More</span></a>
                          @endif </div>
                    </div>
@endforeach

                    

                    <div class="clearfix"></div>
                </div>
            </section>
            <!-- section close -->

            <!-- section begin -->
            <section id="section-cta-2" class="cta light-text" data-stellar-background-ratio="0.5" class="light-text">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="padding30 overlaydark80 wow fadeIn">
                                <div class="row">
                                    <div class="col-md-10">
                                    <h2 class="mb0 mt10">{{$ser->contact}}</h2>
                                    </div>
                                    <div class="col-md-2 text-right">
                                        <a href="{{url('/contact')}}" class="btn btn-custom">Contact Us</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- section close -->

        </div>
    <!-- content close -->

 @endsection
@extends('website_layout.main')

 @section('css')

{{-- <head>
  <title>jQuery lightGallery demo</title> --}}

  <link href="../dist/css/lightgallery.css" rel="stylesheet">
  <style type="text/css">
    body {
      background-color: ;
    }

    .demo-gallery>ul {
      margin-bottom: 0;
    }

    .demo-gallery>ul>li {
      float: left;
      margin-bottom: 15px;
      margin-right: 20px;
      width: 200px;
    }

    .demo-gallery>ul>li a {
      border: 3px solid #FFF;
      border-radius: 3px;
      display: block;
      overflow: hidden;
      position: relative;
      float: left;
    }

    .demo-gallery>ul>li a>img {
      -webkit-transition: -webkit-transform 0.15s ease 0s;
      -moz-transition: -moz-transform 0.15s ease 0s;
      -o-transition: -o-transform 0.15s ease 0s;
      transition: transform 0.15s ease 0s;
      -webkit-transform: scale3d(1, 1, 1);
      transform: scale3d(1, 1, 1);
      height: 100%;
      width: 100%;
    }

    .demo-gallery>ul>li a:hover>img {
      -webkit-transform: scale3d(1.1, 1.1, 1.1);
      transform: scale3d(1.1, 1.1, 1.1);
    }

    .demo-gallery>ul>li a:hover .demo-gallery-poster>img {
      opacity: 1;
    }

    .demo-gallery>ul>li a .demo-gallery-poster {
      background-color: rgba(0, 0, 0, 0.1);
      bottom: 0;
      left: 0;
      position: absolute;
      right: 0;
      top: 0;
      -webkit-transition: background-color 0.15s ease 0s;
      -o-transition: background-color 0.15s ease 0s;
      transition: background-color 0.15s ease 0s;
    }

    .demo-gallery>ul>li a .demo-gallery-poster>img {
      left: 50%;
      margin-left: -10px;
      margin-top: -10px;
      opacity: 0;
      position: absolute;
      top: 50%;
      -webkit-transition: opacity 0.3s ease 0s;
      -o-transition: opacity 0.3s ease 0s;
      transition: opacity 0.3s ease 0s;
    }

    .demo-gallery>ul>li a:hover .demo-gallery-poster {
      background-color: rgba(0, 0, 0, 0.5);
    }

    .demo-gallery .justified-gallery>a>img {
      -webkit-transition: -webkit-transform 0.15s ease 0s;
      -moz-transition: -moz-transform 0.15s ease 0s;
      -o-transition: -o-transform 0.15s ease 0s;
      transition: transform 0.15s ease 0s;
      -webkit-transform: scale3d(1, 1, 1);
      transform: scale3d(1, 1, 1);
      height: 100%;
      width: 100%;
    }

    .demo-gallery .justified-gallery>a:hover>img {
      -webkit-transform: scale3d(1.1, 1.1, 1.1);
      transform: scale3d(1.1, 1.1, 1.1);
    }

    .demo-gallery .justified-gallery>a:hover .demo-gallery-poster>img {
      opacity: 1;
    }

    .demo-gallery .justified-gallery>a .demo-gallery-poster {
      background-color: rgba(0, 0, 0, 0.1);
      bottom: 0;
      left: 0;
      position: absolute;
      right: 0;
      top: 0;
      -webkit-transition: background-color 0.15s ease 0s;
      -o-transition: background-color 0.15s ease 0s;
      transition: background-color 0.15s ease 0s;
    }

    .demo-gallery .justified-gallery>a .demo-gallery-poster>img {
      left: 50%;
      margin-left: -10px;
      margin-top: -10px;
      opacity: 0;
      position: absolute;
      top: 50%;
      -webkit-transition: opacity 0.3s ease 0s;
      -o-transition: opacity 0.3s ease 0s;
      transition: opacity 0.3s ease 0s;
    }

    .demo-gallery .justified-gallery>a:hover .demo-gallery-poster {
      background-color: rgba(0, 0, 0, 0.5);
    }

    .demo-gallery .video .demo-gallery-poster img {
      height: 48px;
      margin-left: -24px;
      margin-top: -24px;
      opacity: 0.8;
      width: 48px;
    }

    .demo-gallery.dark>ul>li a {
      border: 3px solid #04070a;
    }

    .home .demo-gallery {
      padding-bottom: 80px;
    }

    .mar{
      margin-top: 100px;
    }
    .marg{
      padding-top: 50px;
    }
    .custi{
      width: 150px !important;
      height: 200px !important;
       border: 1px solid #ddd;
  border-radius: 4px;
  padding: 5px;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }
  </style>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

  @endsection
  @section('content')
  <!-- subheader begin -->
      {{-- <div class="carousel-inner" >
            
             <div class="carousel-item" >
                <img src="{{asset('storage/'.$ser->sliderimage)}}" alt="..." class="slider" >
                <div class="carousel-caption d-none d-md-block carousel-contents">
                    {!!($ser->image_heading)!!}
                </div>
                </div>

        </div> --}}
       

        {{-- <div class="clearfix"></div> --}}

<div class="container mar">
  <h2 class="marg">Gallery</h2>
  <div class="demo-gallery">
    <ul id="lightgallery" class="list-unstyled row">

      @foreach($gal as $g)
      <li class="col-xs-6 col-sm-4 col-md-3" data-responsive="img/1-375.jpg 375, img/1-480.jpg 480, img/1.jpg 800"
        data-src="{{asset('storage/'.$g->image)}}">
        <a href="">
        <img class="img-responsive custi" src="{{asset('storage/'.$g->image)}}">
        </a>
      </li>
      @endforeach
     
    </ul>
  </div>
</div>
  {{-- <script type="text/javascript">
    $(document).ready(function () {
      $('#lightgallery').lightGallery();
    });
  </script>
  <script src="https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js"></script>
  <script src="../dist/js/lightgallery-all.min.js"></script>
  <script src="../lib/jquery.mousewheel.min.js"></script> --}}
{{-- </body>

</html> --}}
@endsection
@extends('website_layout.main')
@section('css')
 <style>
    #section-welcome-2{background: url('storage/{{$t->sliderimage}}') fixed; background-size:cover;}
    </style>
 @endsection
 @section('content')
   <!-- subheader begin -->
         <div class="carousel-inner" >
            
             <div class="carousel-item" >
                <img src="{{asset('storage/'.$t->sliderimage)}}" alt="..." class="slider" >
                <div class="carousel-caption d-none d-md-block carousel-contents">
                    {!!($t->image_heading)!!}
                   {{-- <a href="{{url('/quote')}}" class="btn btn-custom wow fadeInUp" data-wow-duration="2s">Get A Quote</a> --}}
                </div>
                </div>

        </div>
        <!-- subheader close -->

        <div id="content" class="no-top no-bottom cusa">
            <sectionn>

                <div class="container">
                   <br>
                </div>
            </sectionn>
        </div>

        <!-- section begin -->
        <section id="section-side-4" class="side-bg">
            <div class="col-md-6 col-md-offset-6 pull-right image-container">
                <div class="background-image"></div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-5">

                        <h2>{{$t->page_heading}}</h2>
                        {!!html_entity_decode($t->first_description)!!}
                        <div class="divider-deco"><span></span></div>
                      {!!html_entity_decode($t->second_description)!!}

                    </div>

                    <div class="col-md-5 col-md-offset-2" >
                    <img src="{{asset('storage/'.$t->image)}}" style="height: 450px !important; width:700px !importan" />
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
        </section>
        <!-- section close -->

        <!-- content begin -->
        <div id="content" class="no-padding">

            <!-- section begin -->
            <section id="section-bod" data-stellar-background-ratio="0.5" class="light-text">
                <div class="container">
                    <div class="row">

                        <div class="col-md-12">
                            <h2>Management Team</h2>
                            <div class="divider-deco"><span></span></div>
                        </div>
@foreach($m as $m)
                        <ul class="single-carousel-arrow-nav">
                            <li>
                                <div class="inner">
                                <img src="{{asset('storage/'.$m->image)}}" alt="" class="pull-left" style="height: 250px !important; width:180px !important">
                                    <div class="text">
                                        <span class="name">{{$m->name}} </span>
                                        <span class="position">{{$m->designation}}</span>
                                        {!!html_entity_decode($m->description)!!}
                                    </div>

                                    <div class="clearfix"></div>
                                </div>
                            </li>
                        </ul>

                        <div class="divider-single"></div>

@endforeach

                    </div>
                </div>
            </section>
            <!-- section close -->

        </div>
        <!-- content close -->

 @endsection
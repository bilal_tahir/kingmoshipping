@extends('website_layout.main')
@section('css')
 <style>
    #section-welcome-2{background: url('storage/{{$con->sliderimage}}') fixed; background-size:cover;}
    </style>
 @endsection
 @section('content')
  <!-- subheader begin -->
       <div class="carousel-inner">
            
             <div class="carousel-item">
                <img src="{{asset('storage/'.$con->sliderimage)}}" alt="..." class="slider" >
                <div class="carousel-caption d-none d-md-block carousel-contents">
                    {!!($con->image_heading)!!}
                   {{-- <a href="{{url('/quote')}}" class="btn btn-custom wow fadeInUp" data-wow-duration="2s">Get A Quote</a> --}}
                </div>
                </div>

        </div>
        <!-- subheader close -->



        <!-- content begin -->
        <div id="content" class="no-top no-bottom cusa">
            <section>

                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
@foreach($add as $a)
                            <div class="expand-box location">
                                <div class="inner">

                                    <div class="row">
                                        <div class="col-md-2">
                                            {{$a->office_branch}}
										<h3>{{$a->office_city}}</h3>
                                        </div>
                                        <div class="col-md-2">
                                            <h5>Address</h5>
                                             {{$a->address}}<br>
                                            <span class="btn-arrow btn-open-map"><span class="line"></span><span class="url">View on Map</span></span>

                                        </div>
                                        <div class="col-md-2">
                                            <h5>Phone</h5>
                                             {{$a->phone}}
                                            
                                        </div>
                                        <div class="col-md-2">
                                            <h5>Email</h5>
                                             {{$a->email}}
                                        </div>
                                        <div class="col-md-2">
                                            <h5>Social Media</h5>
                                            <!-- social icons -->
                                            <div class="social">
                                                <a href="#"><i class="fa fa-facebook"></i></a>
                                                <a href="#"><i class="fa fa-twitter"></i></a>
                                            </div>
                                            <!-- social icons close -->
                                        </div>

                                    </div>
                                </div>

                            </div>
                            @endforeach


                        </div>
                    </div>
                </div>

            </section>

            <section id="section-contact-form">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="light-text">
                                <div class="bg-2 padding30">

                                <h2 class="id-color">{{$con->form_heading}}</h2>
                                    <div class="tiny-border"></div>
                                    <p class="lead big">
                                        <i>{{$con->form_text}}</i>
                                    </p>
                                    <div class="text-center">
                                        <img src="img/contact/truck.png" alt="">
                                    </div>
                                </div>
                            </div>

                        </div>
						
                        <div class="col-md-8">
                        <form id="contact-form" class="row form-transparent" method="post" action="{{url('/cmessage')}}" enctype="multipart/form-data">
@csrf
                                <div class="col-md-6">
									<div id="error_email" class="error">Please Input a name</div>
                                    <input type="text" name="name" class="form-control" placeholder="Full Name" required="required">
                                </div>

                                <div class="col-md-6">
                                    <div id="error_email" class="error">Please check your email</div>
                                    <input type="email" name="email" class="form-control" placeholder="Email" required="required">
                                </div>

                                <div class="col-md-6">
                                    <div id="error_email" class="error">Please check your number</div>
                                    <input type="text" name="mobile" class="form-control" placeholder="mobile" required="required">
                                </div>

                                <div class="col-md-12">
                                    <div id="error_message" class="error">Please check your message</div>
                                    <textarea rows="6" name="message" class="form-control" placeholder="Your Message" required="required"></textarea>
                                </div>

                                <div id="mail_success" class="col-md-12 success">Thank you for contacting us. We will be in touch with you very soon.</div>
                                <div id="mail_failed" class="col-md-12 error">Error, email not sent</div>

                                <div class="col-md-12">
                                    <p id="btnsubmit">
										<button type="submit" value="Send message" name="submit" id="submit" class="btn btn-customm fullwidth" title="Submit Your Message!">Send Message</button>
                                    </p>
                                </div>

                            </form>
                        </div>
						
                    </div>
                </div>
            </section>

        </div>
    </div>
    <!-- content close -->

 @endsection
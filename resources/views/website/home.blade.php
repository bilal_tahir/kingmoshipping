 @extends('website_layout.main')
 @section('css')
 <style>
    /* #section-welcome-2{background: url('storage/{{$home->sliderimage}}') fixed; background-size:cover;     height: 750px !important;width: 100% !important } */
    </style>
 @endsection
 @section('content')
  <!-- content begin -->
        <div id="content" class="no-padding">
            <!-- section begin -->
           
        <div class="carousel-inner" style="background-color: #eee !important;">
            
             <div class="carousel-item" style="background-color: #eee !important;">
                <img src="{{asset('storage/'.$home->sliderimage)}}" alt="..." class="homeslider" >
                <div class="carousel-caption d-none d-md-block carousel-content">
                    {!!($home->image_heading)!!}
                   <a href="{{url('/quote')}}" class="btn btn-custom wow fadeInUp" data-wow-duration="2s">Get A Quote</a>
                </div>
                </div>

        </div>
            <!-- section close -->

            <section class="no-padding cusm height90px bg111111 mobile-hide">
                <div class="container">
                    <div class="row-fluid">
                        <div class="col-md-4">
                            <div class="info-box padding20">
                                <i class="fa fa-clock-o"></i>
                                <div class="info-box_text">
                                    <div class="info-box_title">Opening Times</div>
                                    <div class="info-box_subtite">{{$home->timing}}</div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="info-box padding20">
                                <i class="fa fa-home"></i>
                                <div class="info-box_text">
                                    <div class="info-box_title">Our Location</div>
                                    <div class="info-box_subtite">{{$home->location}}</div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="info-box padding20">
                                <i class="fa fa-phone"></i>
                                <div class="info-box_text">
                                    <div class="info-box_title">Our Phone</div>
                                    <div class="info-box_subtite">{{$home->phone}}</div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4"></div>
                    </div>
                </div>
            </section>

            <!-- section begin -->
            <section id="section-side-1" class="side-bg">
                <div class="col-md-6 col-md-offset-6 pull-right image-container">
                    <div class="background-image"></div>
                </div>

                <div class="container">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="text-center">

{!!html_entity_decode($home->about_content)!!}
                                <a href="{{url('/about')}}" class="btn btn-custom">Read More</a>
                                

                            </div>

                        </div>

                        <div class="col-md-5 col-md-offset-2 light-text">
                            <div class="text-center">
                            <img src="{{asset('storage/'.$home->about_image)}}" alt="" class="wow slideInUp homeimg"  />
                            </div>

                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
            </section>

            <!-- section begin -->
           
            <section class="no-padding">
                <div class="container-fullwidth">

                     @foreach($homefreight as $freight)
                    <div class="box-service one-fourth">

                        <div class="bg-color-fx padding-5 text-center">
                        <h3>{{$freight->heading}}</h3>
                            <div class="tiny-border margintop10 marginbottom10"></div>
                            <img src="{{asset('storage/'.$freight->image)}}" class="img-responsive margintop20 marginbottom20 wow fadeInRight" alt="" style=""/>
                             {!!html_entity_decode($freight->text)!!}
                            @if($freight->id == 1)
                        <a href="{{url('/landfreight')}}" class="btn-arrow hover-light"><span class="line"></span><span class="url">Read More</span></a>
                       @endif
                        @if($freight->id == 2)
                        <a href="{{url('/airfreight')}}" class="btn-arrow hover-light"><span class="line"></span><span class="url">Read More</span></a>
                        @endif  
                        @if($freight->id == 3)
                        <a href="{{url('/seafreight')}}" class="btn-arrow hover-light"><span class="line"></span><span class="url">Read More</span></a>
                        @endif    
                        @if($freight->id == 4)
                        <a href="{{url('/solution')}}" class="btn-arrow hover-light"><span class="line"></span><span class="url">Read More</span></a>
                          @endif
                    </div>
                    </div>

                    @endforeach

               
                    <div class="clearfix"></div>
                </div>
            </section>
            <!-- section close -->
            <!-- section close -->

            <!-- section begin -->
            <!-- section begin -->
            <section id="section-bod" data-stellar-background-ratio="0.5" class="light-text">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Management Team</h2>
                            <div class="divider-deco"><span></span></div>
                        </div>
@foreach($hometeam as $team)
                        <!-- team profile -->
                        <div class="col-md-4 text-center">
                            <div class="bg111111">
                                <div class="row">
                                    <div class="col-md-6 pr0">
                                    <img src="{{asset('storage/'.$team->image)}}" alt="" class="img-responsive" style="height: 280px !important; width:180px !important">
                                    </div>
                                    <div class="col-md-6">
                                        <div class="p20 pl10 pt30">
                                            <div class="id-color font2"><i>{{$team->name}}</i></div>
                                            <div class="font2 mb10 text-white"><i>{{$team->designation}}</i></div>
                                            {{$team->description}}

                                            <!-- Read More -->
                                            <div class="social hspace10 mt10">
                                            <a href="{{url('/team')}}" class="btn-arrow id-color hover-light"><span class="line"></span><span class="url">Read More</span></a>
                                            </div>
                                            <!-- Read More Close -->
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>

                   @endforeach
                     


                    </div>
                </div>
            </section>
            <!-- section close -->

 @endsection
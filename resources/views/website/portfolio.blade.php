@extends('website_layout.main')
@section('css')
 <style>
    #subheader.page-client{background: url('storage/{{$por->sliderimage}}') fixed; background-size:cover; }
    </style>
 @endsection
 @section('content')
    <!-- subheader begin -->
      <div class="carousel-inner" >
            
             <div class="carousel-item" >
                <img src="{{asset('storage/'.$por->sliderimage)}}" alt="..." class="slider" >
                <div class="carousel-caption d-none d-md-block carousel-contents">
                    {!!($por->image_heading)!!}
                   {{-- <a href="{{url('/quote')}}" class="btn btn-custom wow fadeInUp" data-wow-duration="2s">Get A Quote</a> --}}
                </div>
                </div>

        </div>
        <!-- subheader close -->

        <div class="clearfix"></div>

        <!-- content begin -->
        <div id="content" class="no-padding cusa">

            <!-- section begin -->
            <section>
                <div class="container">
                    <div class="row">
						<div class="col-md-12">
						<div class="row">

                             {!!html_entity_decode($por->content)!!}

                             @foreach($cl as $c)
							<div class="col-md-2 mb30">
                            <img src="{{asset('storage/'.$c->image)}}" class="img-responsive" alt="" style="height:120px !important; width:150px !important">
							</div>
							@endforeach
							
						</div>
					</div>
                        
                    </div>


                </div>
            </section>
			
			<section class="bg-grey">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<h2>TESTIMONIES</h2>
							<div class="divider-deco"><span></span></div>
							<div id="testi-carousel-2" class="testi-slider wow fadeIn" data-wow-delay="0s" data-wow-duration="1s">

                                @foreach($tes as $t)
                                <div class="item">
                                    <blockquote>
                                       
{{$t->description}}
                                    </blockquote>
                                    <div class="arrow-down"></div>
                                    <div class="testi-by">
                                    <img src="{{asset('storage/'.$t->image)}}" class="img-circle" alt="">
                                    <span class="name"><strong>{{$t->heading}}</strong></span>
                                    </div>
                                </div>

                              @endforeach

                                
                             

                              

                            </div>
                        </div>
					
                        <div class="col-md-6">
                        <h2> {{$por->about_heading}}</h2>
							{!!html_entity_decode($por->first_description)!!}
							<div class="divider-deco"><span></span></div>
							{!!html_entity_decode($por->second_description)!!}
                        <a href="{{url('/about')}}" class="btn btn-custom">Read More</a>
						
						</div>
					</div>
				</div>
			</section>

            <!-- section begin -->
            <section id="section-cta-2" class="cta light-text" data-stellar-background-ratio="0.5" class="light-text">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="padding30 overlaydark80 wow fadeIn">
                                <div class="row">
                                    <div class="col-md-10">
                                        <h2 class="mb0 mt10">{{$por->contact}}</h2>
                                    </div>
                                    <div class="col-md-2 text-right">
                                    <a href="{{url('/contact')}}" class="btn btn-custom">Contact Us</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- section close -->

        </div>
        <!-- content close -->

 @endsection
<?php

namespace App\Http\Controllers;

use App\HomeFreight;
use Illuminate\Http\Request;

class HomeFreightController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\HomeFreight  $homeFreight
     * @return \Illuminate\Http\Response
     */
    public function show(HomeFreight $homeFreight)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HomeFreight  $homeFreight
     * @return \Illuminate\Http\Response
     */
    public function edit(HomeFreight $homeFreight)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HomeFreight  $homeFreight
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HomeFreight $homeFreight)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HomeFreight  $homeFreight
     * @return \Illuminate\Http\Response
     */
    public function destroy(HomeFreight $homeFreight)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;
use App\Logo;
use App\Home;
use App\HomeFreight;
use App\HomeTeam;
use App\ServicePage;
use App\Address;
use App\Contact;
use App\Quote;
use App\TeamContent;
use App\TeamMember;
use App\Testimonials;
use App\PortfolioContent;
use App\ClientImage;
use App\AboutContent;
use App\ContactMessage;
use App\QuoteMessage;
use Redirect;
use App\Gallery;
use Illuminate\Http\Request;

class WebsiteController extends Controller
{
  public function index()
  {
      $logo = Logo::where('id',1)->first();
      $home = Home::where('id',1)->first();
      $homefreight = HomeFreight::whereBetween('id', [1, 4])->get();
     
      $hometeam = Hometeam::whereBetween('id', [1, 3])->get();
    //    dd($hometeam[0]->name);
    //   dd($logo->logo);
      return view ('website.home')->with([
          'logo' => $logo,
          'home' => $home,
          'homefreight' => $homefreight,
          'hometeam' => $hometeam,
      ]);
  }
   public function about()
  {
      $logo = Logo::where('id',1)->first();
      $ab = AboutContent::where('id',1)->first();
      return view ('website.about')->with([
          'logo' => $logo,
          'ab' => $ab,
      ]);
  }
   public function services()
  {
      $logo = Logo::where('id',1)->first();
      $ser = ServicePage::where('id',1)->first();
       $freight = HomeFreight::all();
      return view ('website.services')->with([
          'logo' => $logo,
          'ser' => $ser,
          'freight' => $freight,
      ]);
  }
   public function contact()
  {
      $logo = Logo::where('id',1)->first();
      $add = Address::all();
      $con = Contact::where('id',1)->first();
      return view ('website.contact')->with([
          'logo' => $logo,
          'add' => $add,
          'con' => $con,
      ]);
  }
   public function portfolio()
  {
      $logo = Logo::where('id',1)->first();
      $por = PortfolioContent::where('id',1)->first();
      $tes = Testimonials::all();
      $cl = ClientImage::all();
      return view ('website.portfolio')->with([
          'logo' => $logo,
          'por' => $por,
          'tes' => $tes,
          'cl' => $cl,
      ]);
  }
   public function team()
  {
      $logo = Logo::where('id',1)->first();
      $t = TeamContent::where('id',1)->first();
      $m = TeamMember::all();
      return view ('website.team')->with([
          'logo' => $logo,
          't' => $t,
          'm' => $m,
      ]);
  }
   public function landfreight()
  {
      $logo = Logo::where('id',1)->first();
      $ser = ServicePage::where('id',2)->first();
       $freight = HomeFreight::where('id','<>',1)->get(); 
    //    dd($freight->count());
      return view ('website.landfreight')->with([
          'logo' => $logo,
          'ser' => $ser,
          'freight' => $freight,
      ]);
  }
   public function airfreight()
  {
      $logo = Logo::where('id',1)->first();
           $ser = ServicePage::where('id',3)->first();
       $freight = HomeFreight::where('id','<>',2)->get(); 
      return view ('website.airfreight')->with([
          'logo' => $logo,
           'ser' => $ser,
          'freight' => $freight,
      ]);
  }
   public function seafreight()
  {
      $logo = Logo::where('id',1)->first();
           $ser = ServicePage::where('id',4)->first();
       $freight = HomeFreight::where('id','<>',3)->get(); 
      return view ('website.seafreight')->with([
          'logo' => $logo,
           'ser' => $ser,
          'freight' => $freight,
      ]);
  }
   public function solution()
  {
      $logo = Logo::where('id',1)->first();
           $ser = ServicePage::where('id',5)->first();
       $freight = HomeFreight::where('id','<>',4)->get(); 
      return view ('website.solution')->with([
          'logo' => $logo,
           'ser' => $ser,
          'freight' => $freight,
      ]);
  }
   public function package()
  {
      $logo = Logo::where('id',1)->first();
           $ser = ServicePage::where('id',6)->first();
       $freight = HomeFreight::where('id','<>',5)->get(); 
       $gal = Gallery::all();
    
      return view ('website.package')->with([
          'logo' => $logo,
           'ser' => $ser,
          'freight' => $freight,
          'gal' => $gal,
      ]);
  }
   public function quote()
  {
      $logo = Logo::where('id',1)->first();
      $q = Quote::where('id',1)->first();
      return view ('website.quote')->with([
          'logo' => $logo,
          'q' => $q,
      ]);
  }
  public function cmessage(Request $message)
    {
     $con = new ContactMessage();
     
     $con->name = $message['name'];
     $con->email = $message['email'];
     $con->mobile = $message['mobile'];
     $con->message = $message['message'];
     $con->save();

      return Redirect::back();
    }
      public function qmessage(Request $message)
    {
      $con = new QuoteMessage();
     
     $con->name = $message['name'];
     $con->email = $message['email'];
     $con->mobile = $message['mobile'];
     $con->message = $message['message'];
     $con->save();

      return Redirect::back();   
    }
}

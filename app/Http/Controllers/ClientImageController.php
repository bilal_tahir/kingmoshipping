<?php

namespace App\Http\Controllers;

use App\ClientImage;
use Illuminate\Http\Request;

class ClientImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ClientImage  $clientImage
     * @return \Illuminate\Http\Response
     */
    public function show(ClientImage $clientImage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ClientImage  $clientImage
     * @return \Illuminate\Http\Response
     */
    public function edit(ClientImage $clientImage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ClientImage  $clientImage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ClientImage $clientImage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ClientImage  $clientImage
     * @return \Illuminate\Http\Response
     */
    public function destroy(ClientImage $clientImage)
    {
        //
    }
}

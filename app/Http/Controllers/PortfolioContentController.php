<?php

namespace App\Http\Controllers;

use App\PortfolioContent;
use Illuminate\Http\Request;

class PortfolioContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PortfolioContent  $portfolioContent
     * @return \Illuminate\Http\Response
     */
    public function show(PortfolioContent $portfolioContent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PortfolioContent  $portfolioContent
     * @return \Illuminate\Http\Response
     */
    public function edit(PortfolioContent $portfolioContent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PortfolioContent  $portfolioContent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PortfolioContent $portfolioContent)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PortfolioContent  $portfolioContent
     * @return \Illuminate\Http\Response
     */
    public function destroy(PortfolioContent $portfolioContent)
    {
        //
    }
}

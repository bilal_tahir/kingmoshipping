<?php

namespace App\Http\Controllers;

use App\ServicePage;
use Illuminate\Http\Request;

class ServicePageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ServicePage  $servicePage
     * @return \Illuminate\Http\Response
     */
    public function show(ServicePage $servicePage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ServicePage  $servicePage
     * @return \Illuminate\Http\Response
     */
    public function edit(ServicePage $servicePage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ServicePage  $servicePage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ServicePage $servicePage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ServicePage  $servicePage
     * @return \Illuminate\Http\Response
     */
    public function destroy(ServicePage $servicePage)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\ServiceSlider;
use Illuminate\Http\Request;

class ServiceSliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ServiceSlider  $serviceSlider
     * @return \Illuminate\Http\Response
     */
    public function show(ServiceSlider $serviceSlider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ServiceSlider  $serviceSlider
     * @return \Illuminate\Http\Response
     */
    public function edit(ServiceSlider $serviceSlider)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ServiceSlider  $serviceSlider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ServiceSlider $serviceSlider)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ServiceSlider  $serviceSlider
     * @return \Illuminate\Http\Response
     */
    public function destroy(ServiceSlider $serviceSlider)
    {
        //
    }
}

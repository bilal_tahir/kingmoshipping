<?php

namespace App\Http\Controllers;

use App\TeamContent;
use Illuminate\Http\Request;

class TeamContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TeamContent  $teamContent
     * @return \Illuminate\Http\Response
     */
    public function show(TeamContent $teamContent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TeamContent  $teamContent
     * @return \Illuminate\Http\Response
     */
    public function edit(TeamContent $teamContent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TeamContent  $teamContent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TeamContent $teamContent)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TeamContent  $teamContent
     * @return \Illuminate\Http\Response
     */
    public function destroy(TeamContent $teamContent)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\QuoteMessage;
use Illuminate\Http\Request;

class QuoteMessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\QuoteMessage  $quoteMessage
     * @return \Illuminate\Http\Response
     */
    public function show(QuoteMessage $quoteMessage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\QuoteMessage  $quoteMessage
     * @return \Illuminate\Http\Response
     */
    public function edit(QuoteMessage $quoteMessage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\QuoteMessage  $quoteMessage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QuoteMessage $quoteMessage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\QuoteMessage  $quoteMessage
     * @return \Illuminate\Http\Response
     */
    public function destroy(QuoteMessage $quoteMessage)
    {
        //
    }

    
}

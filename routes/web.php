<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/','WebsiteController@index');
Route::get('/about','WebsiteController@about');
Route::get('/services','WebsiteController@services');
Route::get('/contact','WebsiteController@contact');
Route::get('/portfolio','WebsiteController@portfolio');
Route::get('/team','WebsiteController@team');
Route::get('/landfreight','WebsiteController@landfreight');
Route::get('/airfreight','WebsiteController@airfreight');
Route::get('/seafreight','WebsiteController@seafreight');
Route::get('/solution','WebsiteController@solution');
Route::get('/package','WebsiteController@package');
Route::get('/quote','WebsiteController@quote');
Route::post('/cmessage','WebsiteController@cmessage');
Route::post('/qmessage','WebsiteController@qmessage');


Route::get('/linkstorage', function () {
    Artisan::call('storage:link');
});




Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePortfolioContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolio_contents', function (Blueprint $table) {
            $table->id();
            $table->string('sliderimage');
            $table->string('image_heading');
            $table->longText('content');
            $table->string('about_heading');
            $table->longText('first_description');
            $table->longText('second_description');
            $table->string('contact');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portfolio_contents');
    }
}

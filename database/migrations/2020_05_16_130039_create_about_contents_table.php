<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAboutContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('about_contents', function (Blueprint $table) {
            $table->id();
            $table->string('sliderimage');
            $table->string('image_heading');
            $table->string('first_section_heading');
            $table->longText('first_section_content');
            $table->string('second_section_heading');
            $table->longText('second_section_content');
            $table->string('second_section_image');
            $table->string('third_section_heading');
            $table->longText('third_section_content');
            $table->string('bottom_heading');
            $table->longText('bottom_first_description');
            $table->longText('bottom_second_description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('about_contents');
    }
}

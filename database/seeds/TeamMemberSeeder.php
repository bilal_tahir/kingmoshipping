<?php

use Illuminate\Database\Seeder;

class TeamMemberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
             DB::table('team_members')->insert([
            'image' => 'team_1.jpg', 
             'name' => 'Ben JABLY',  
            'description' => '<p>
                                            He holds a BSC in Operations and Supply Chain, Diploma in Ports and Shipping Management
                                            and Certificate In Logistics and Transportation. He has over ten years experience in the
                                            industry. His relational networking skills has won us many clients.</p>',  
            'designation' => 'GENERAL MANAGER',    
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class AddressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('addresses')->insert([
            'office_branch' => 'Head Office:', 
             'office_city' => 'Tema, Ghana',  
            'address' => 'Ground Floor Royal Building, Community Four Tema.',  
            'phone' => '+233 303 217 108', 
            'email' => 'info@kosshipping.com',    
        ]);
        DB::table('addresses')->insert([
            'office_branch' => 'Branch Office:', 
             'office_city' => 'Accra, Ghana',  
            'address' => '  Kanda Kade Avenue, Opposite GNTC Building Accra..',  
            'phone' => '+233 307 031 180', 
            'email' => 'info@kosshipping.com',    
        ]);
    }
}

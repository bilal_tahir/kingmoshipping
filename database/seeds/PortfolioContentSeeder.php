<?php

use Illuminate\Database\Seeder;

class PortfolioContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          DB::table('portfolio_contents')->insert([
            'sliderimage' => 'bg_about.jpg',
             'image_heading' => 'Client Portfolio', 
            'content' => '<p><b>
                                Through our constant pursuit of building mutually beneficial relationships, we have
                                built a strong clientele based comprising of individuals and corporate organisations.
                                Some of our clients include;</b>
                            </p>', 
            'about_heading' => 'About Our Company',
            'first_description' => '<p><b>Your imagination is our reality. Imagine a world where your imports and exports, cargo handling, courier services, warehousing, transportation and transit challenges are solved before they even materialise.</b> </p>
							', 
            'second_description' => '<p>Kos Shipping offers integrated logistics services and tailored, customer-focused solutions for
                                imports and exports, cargo handling, courier services, warehousing, transportation and transit challenges.
                            </p>
                            <P>
                                CUSTOMERS are our hallmark. We therefore access and analyze their unique requirements and demands in order
                                to offer them the logistical design that best suits their needs.
                            </p>', 
                             'contact' => 'Contact us now to get a quote for all your shipping needs.',    
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class ServicePageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
           DB::table('service_pages')->insert([
            'page_name' => 'services',
            'sliderimage' => 'bg_services_1.jpg', 
            'image_heading' => 'Our Services', 
            'page_heading' => '',
            'first_description' => '',
            'second_description' => '',
            'contact' => 'Contact us now to get a quote for all your shipping needs.',    
        ]);
         DB::table('service_pages')->insert([
            'page_name' => 'landfreight',
            'sliderimage' => 'bg_services_1.jpg', 
            'image_heading' => 'Land FReight Service', 
            'page_heading' => 'Land Freight',
            'first_description' => ' <p class="lead">KOS Shipping recognizes that a good transport system that will ensure the cargos are delivered timely
                                and in a safe and secured condition as a strict requirement for the client. In order for KOS Shipping to ensure this
                                is achieved at all times;
                            </p>',
            'second_description' => '   <p>
                                We will employ the use of quality clean trucks/equipment, drivers and assistants are always in personal protective equipment.
                            </p>
                            <p>
                                Our effective and professional transport management team will ensure proper planning and execution of all customer movements.
                            </p>
                            <p>
                                We will endeavor to ensure our transport system is well managed so that customer’s goods are delivered to advise final destinations
                                at the right times, in the right quantity and in good condition.
                                KOS Shipping will ensure that trucks are available at all times to meet our clearance times as well as delivery. Trucks will be
                                specifically chosen to ensure the safety and security of the cargo.

                            </p>',
            'contact' => 'Contact us now to get a quote for all your shipping needs.',    
        ]);
         DB::table('service_pages')->insert([
            'page_name' => 'airfreight',
            'sliderimage' => 'bg_services_1.jpg', 
            'image_heading' => 'Air Freight Service', 
            'page_heading' => 'Air Freight',
            'first_description' => '<p><b>KOS Shipping will provide Airfreight service using the most price effective
                                    method.</b>
                            </p>',
            'second_description' => ' <p>
                                All waybills will reflect KOS Shipping origins as the notify party for the Goods in transit.
                                Services Offered:
                            </p>',
            'contact' => 'Contact us now to get a quote for all your shipping needs.',    
        ]);
         DB::table('service_pages')->insert([
            'page_name' => 'seafreight',
            'sliderimage' => 'bg_services_1.jpg', 
            'image_heading' => 'Sea Freight Service', 
            'page_heading' => 'Sea Freight',
            'first_description' => '<p><b>With our broad range of Ocean Freight products covering different equipment types and consolidation services,
                                we ensure your cargo reaches the right place, at the right time in a cost-efficient way.
                            </b></p>',
            'second_description' => ' <p>
                                We work with a spread of ocean carriers covering major carrier alliances with planned space protection from every major container
                                port in the world to deliver reliability.
                            </p>
                            <p>
                                Naturally, our expertise also includes focused and professional handling of all conventional cargo transportation.
                            </p>',
            'contact' => 'Contact us now to get a quote for all your shipping needs.',    
        ]);
         DB::table('service_pages')->insert([
            'page_name' => 'freightservice',
            'sliderimage' => 'bg_services_1.jpg', 
            'image_heading' => 'Freight Forwarding Service', 
            'page_heading' => 'Freight Forwarding Solutions',
            'first_description' => '<p><b>
                                At KOS Shipping we understand that operations and requirements of all companies are unique.
                                A unique Freight Forwarding Solutions will be designed and agreed on prior go live of all our
                                engagements between you and KOS Shipping.</b>
                            </p>',
            'second_description' => '   <p>
                                KOS Shipping understands the Standard Operating Procedures and appreciates that short clearance time
                                and a safe and secure clearance is essential to customers. Our objectives are to ensure that our client’s
                                cargo is cleared within 4 working days of vessel’s arrival/discharge, within the Laws and Regulations of
                                the Country. KOS Shipping has been handling customs clearance in Ghana for over 5years and all customs
                                clearance are done by our own in-house agents, and this ensures we have full control.
                            </p>
                            <p>
                                KOS Shipping and Logistics also understands that knowledge and understanding of documentation of import/export
                                of telecommunications equipment is important. As such we take time to abreast ourselves of HS codes and duty regimes
                                for all telecommunications equipment. We proactively liaise with customer to abreast ourselves of frequently imported
                                cargo to ensure error free document and correct Import Duty, VAT, Import Excise and others applied. At KOS Shipping and
                                Logistics we have a dedicated documentation team headed by our Operations Director with over 25years experience in logistics
                                documentation spanning all industries and cargo types. The team’s experience will be put to bear on Customer’s logistics needs.
                                Our documentation desk will be proactively involved to ensure there are no challenges in documentation requirements and processing
                                so as not to delay clearance.
                            </p>',
            'contact' => 'Contact us now to get a quote for all your shipping needs.',    
        ]);
         DB::table('service_pages')->insert([
            'page_name' => 'travelpackage',
            'sliderimage' => 'bg_services_1.jpg', 
            'image_heading' => 'Business Travel Package Service', 
            'page_heading' => 'Business Travel Package',
            'first_description' => '<p><b>We realize the stress business people go through just to embark on a business trip.</b>
                            </p>',
            'second_description' => ' <p>
                                KOS Shipping provides an all-round business trip package to destinations like Dubai, China, Mauritius,
                                South Africa, India and Turkey. Our service package includes Visa, Ticket, Tour, Reservation and Freight Forwarding.
                            </p>
                            <p>
                                Our Partners in Dubai, China and other parts of the world assists our clients to have a successful business trip and
                                offers a freight forwarding solution whiles KOS Shipping will be ready to provide clearing services.
                            </p>',
            'contact' => 'Contact us now to get a quote for all your shipping needs.',    
        ]);
    }
}

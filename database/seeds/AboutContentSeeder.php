<?php

use Illuminate\Database\Seeder;

class AboutContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('about_contents')->insert([
            'sliderimage' => 'bg_about.jpg',
            'image_heading' => 'About Us',  
            'first_section_heading' => 'Who We Are', 
            'first_section_content' => '  <p>Kos Shipping offers integrated logistics services and tailored, customer-focused solutions for imports and exports, cargo handling, courier services, warehousing, transportation and transit challenges.</p>
                            <p>CUSTOMERS are our hallmark. We therefore access and analyze their unique requirements and demands in order to offer them the logistical design that best suits their needs. We have pieced the pieces together to design solutions that create value for our CUSTOMERS. We challenge ourselves always to ensure convenience for our CUSTOMERS by delivering outstanding services.</p>
                            <p>We are using our experience and the power of ICT to create convenience for our customers. We aim to build on our established long-term relationships with our existing customers, while attracting new ones. Our goal is to be your trusted partner.</p>
',
            'second_section_heading' => 'KOS Commitment',
            'second_section_content' => '<p>
                                        <i>Our zeal to succeed and to reach the pinnacle of our potential explains our unquestionable commitment to our most valued customers.</i>
                                    </p>',
           
                                   'second_section_image' => 'committment-image.png',
                                    'third_section_heading' => 'Mission & Vision',
            'third_section_content' => '<h4>Mission</h4>
                            The KOS mission is to develop innovative logistics solutions that are economical, convenient and personalized to suit your needs and demands.
                            <h4>Vision</h4>
                            KOS Shipping has a vision to be “A Household Name.',
            'bottom_heading' => 'The KOS WAY',
            'bottom_first_description' => '<p><b>Your imagination is our reality. Imagine a world where your imports and exports,
                                            cargo handling, courier services, warehousing, transportation and transit challenges
                                            are solved before they even materialise.</b>
                                        </p>',
            'bottom_second_description' => '   <p>
                                            It takes vision, strategy and a dedicated team to achieve that. The KOS mission is to
                                            develop innovative logistics solutions that are economical, convenient and personalised
                                            to suit your needs and demands.
                                        </p>
                                        <p>
                                            KOS Shipping has a vision to be “A Household Name.” We want to be the best in what
                                            we do to create and maximise value for our customers. At KOS Shipping, You are our
                                            reason. Our team is at your service. Your business and your time is what we breathe.
                                            And so we take pride in serving You.
                                        </p>

                                    

                                        <ul>
                                            <p><strong>At KOS these are the values we strive for: </strong></p>
                                            <li>We strive to meet the expectations of customers, employees and shareholders.</li>
                                            <li>We seek to continuously increase our corporate value.</li>
                                            <li>We think locally and act from a global perspective.</li>
                                            <li>We respect diversity and support individual growth.</li>
                                            <li>We seek to be your valued and trusted Partner.</li>
                                            <li>We build mutually beneficial relationships.</li>
                                            <li>We enhance the reputation of our customers through efficient delivery of services.</li>
                                        </ul>',
            

        ]);
    }
}

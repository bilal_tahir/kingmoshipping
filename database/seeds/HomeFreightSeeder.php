<?php

use Illuminate\Database\Seeder;

class HomeFreightSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('home_freights')->insert([
            
            'heading' => 'Land Freight',  
             'image' => '8.png',
            'text' => '<p>KOS Shipping recognizes that a good transport system that will ensure the cargos are ... </p>',    
        ]);
         DB::table('home_freights')->insert([
            
            'heading' => 'Air Freight',  
             'image' => '2.png',
            'text' => ' <p>KOS Shipping and Logistics will provide Airfreight service using the most price effective method ...</p>
                            ',    
        ]);
         DB::table('home_freights')->insert([
            
            'heading' => 'Sea Freight',  
             'image' => '1.png',
            'text' => '  <p>With our broad range of Ocean Freight products covering different equipment types and consolidation services ...</p>
                             ',    
        ]);
         DB::table('home_freights')->insert([
            
            'heading' => 'Freight Forwarding Solutions',  
             'image' => '6.png',
            'text' => ' <p>At KOS Shipping we understand that operations and requirements of all companies are unique ...</p>
                           ',    
        ]);
          DB::table('home_freights')->insert([
            
            'heading' => 'Business Travel Package',  
             'image' => '7.png',
            'text' => '   <p>We realize the stress business people go through just to embark on a business trip. ...</p>
                           ',    
        ]);
    }
    }


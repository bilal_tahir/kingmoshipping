<?php

use Illuminate\Database\Seeder;

class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contacts')->insert([
            'sliderimage' => 'bg_services_1.jpg', 
             'image_heading' => 'Contact Us',  
            'form_heading' => 'GET IN TOUCH WITH US',  
            'form_text' => 'For anything from business inquiries about our services to general questions about KOS, contact us by filling the form or call our specific offices.',    
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class TestimonialsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('testimonials')->insert([
            
             'description' => 'We are satisfied with KOS Shipping communication skills and fast way of handling clearing procedures.', 
            'image' => 'testimonial-1.jpg', 
            'heading' => 'WINDSOL ENERGY LIMITED',    
        ]);
    }
}

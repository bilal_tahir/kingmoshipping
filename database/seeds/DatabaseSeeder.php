<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->call(LogoSeeder::class);
         $this->call(HomeSeeder::class);
         $this->call(HomeTeamSeeder::class);
         $this->call(HomeFreightSeeder::class);
         $this->call(ServicePageSeeder::class);
         $this->call(AddressSeeder::class);
         $this->call(ContactSeeder::class);
          $this->call(QuoteSeeder::class);
        $this->call(TeamContentSeeder::class);
        $this->call(TeamMemberSeeder::class); 
        $this->call(TestimonialsSeeder::class); 
        $this->call(PortfolioContentSeeder::class);
        $this->call(ClientImageSeeder::class); 
         $this->call(AboutContentSeeder::class);  
    }
}

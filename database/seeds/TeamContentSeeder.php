<?php

use Illuminate\Database\Seeder;

class TeamContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('team_contents')->insert([
            'sliderimage' => 'bg_team.jpg',
             'image_heading' => 'Meet The Team', 
            'page_heading' => 'Our Team', 
            'first_description' => '<p><b>Our core competence is an enthusiastic team.</b></p>',  
            'second_description' => ' <p>
                            The KOS team is made up of individuals with diverse skills to fulfil
                            the needs and aspirations of our customers. Trained to the highest
                            standards, we have all the experience and exposure in the Shipping
                            and Freight forwarding industry. And with support from progressive
                            and innovative information systems, we ensure you and your
                            business have most efficient and effective services.
                        </p>',
            'image' => 'bg_side_3.jpg',  
        ]);
    }
}

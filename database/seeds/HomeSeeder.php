<?php

use Illuminate\Database\Seeder;

class HomeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('homes')->insert([
            'sliderimage' => 'bg_slider_2.jpg',
            'image_heading' => '<h2>Our Mission is to provide</h2></span><br>
                                        INNOVATIVE<br>
                                        LOGISTICS SOLUTIONS.',
            'timing' => 'Mondays - Fridays: 08:00am - 5:00pm',
            'location' => 'Royal Building, Comm. 4 Tema.',
            'phone' => '+233 303 217 108',
            'about_content' => '                      <h2>THE KOS WAY</h2>
                                <p class="lead">
                                    Your imagination is our reality. Imagine a world where your imports and exports,
                                    cargo handling, courier services, warehousing, transportation and transit challenges
                                    are solved before they even materialise.
                                </p>
                                <div class="divider-deco"><span></span></div>
                                <p>
                                    It takes vision, strategy and a dedicated team to achieve that. The KOS mission is to
                                    develop innovative logistics solutions that are economical, convenient and personalised
                                    to suit your needs and demands.
                                    KOS Shipping has a vision to be “A Household Name.” We want to be the best in what
                                    we do to create and maximise value for our customers.

                                    <br>
                                    <br>
</p>',
            'about_image' => 'imac.png',    
        ]);
    }
}

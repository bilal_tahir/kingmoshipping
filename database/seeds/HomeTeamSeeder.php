<?php

use Illuminate\Database\Seeder;

class HomeTeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('home_teams')->insert([
            'name' => 'Ben Jably,', 
             'image' => 'team_1.jpg',  
            'designation' => 'General Manager',  
            'description' => 'He holds a BSC in Operations and Supply Chain, Diploma in Ports and Shipping ...',    
        ]);
        DB::table('home_teams')->insert([
            'name' => 'Bernard Djabanor,', 
             'image' => 'team_2.jpg',  
            'designation' => 'Admin & Fin Man',  
            'description' => ' Holds an MA in Ports and Shipping Administration from the Regional Maritime University ...
',    
        ]);
        DB::table('home_teams')->insert([
            'name' => 'Elvis Avornyo,', 
             'image' => 'team_3.jpg',  
            'designation' => 'Admin. & Opera. Off.',  
            'description' => ' Holds a Bachelor Degree in Business Administration (BBA) in Marketing from Valley View ...
',    
        ]);
    }
}

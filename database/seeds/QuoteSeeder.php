<?php

use Illuminate\Database\Seeder;

class QuoteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('quotes')->insert([
            'sliderimage' => 'bg_services_1.jpg', 
             'image_heading' => 'Quotes',  
            'form_heading' => 'GET IN TOUCH WITH US',  
            'form_text' => 'For anything from business inquiries about our services to general questions about KOS, contact us by filling the form or call our specific offices.',    
        ]);
    }
}
